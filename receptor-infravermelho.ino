
// biblioteca infravermelho
#include <IRremote.h>

// define os pinos de cada componente no controlador
int pinoReceptor = 2;
int led = 13;

// objeto da biblioteca de infravermelho
IRrecv irrecv(pinoReceptor);

// armazena o resultado do receptor de infravermelho
decode_results results;

void setup() {
  pinMode(led, OUTPUT);
  Serial.begin(9600);
  irrecv.enableIRIn();
}

void loop() {
  if (irrecv.decode(&results)) {
    digitalWrite(led, HIGH);
    Serial.println(results.value, HEX);
    dump(&results);
    irrecv.resume(); // Lê mais valores caso existam
  }
  digitalWrite(led, LOW);
  // aumentar volume E17AB04F - valor coletado do controle tv apenas para teste
  // abaixar volume E17A708F - valor coletado do controle de tv apenas para teste
}

void dump(decode_results *results) {
  int count = results->rawlen;
  if (results->decode_type == UNKNOWN) {
    Serial.println("Não foi possível de decodificar a mensagem");
  }
  else {
    if (results->decode_type == NEC) {
      Serial.print("Decoded NEC: ");
    }
    else if (results->decode_type == SONY) {
      Serial.print("Decoded SONY: ");
    }
    else if (results->decode_type == RC5) {
      Serial.print("Decoded RC5: ");
    }
    else if (results->decode_type == RC6) {
      Serial.print("Decoded RC6: ");
    }
    Serial.print(results->value, HEX);
    Serial.print(" (");
    Serial.print(results->bits, DEC);
    Serial.println(" bits)");
  }
  Serial.print("Raw (");
  Serial.print(count, DEC);
  Serial.print("): ");

  for (int i = 0; i < count; i++) {
    if ((i % 2) == 1) {
      Serial.print(results->rawbuf[i]*USECPERTICK, DEC);
    }
    else {
      Serial.print(-(int)results->rawbuf[i]*USECPERTICK, DEC);
    }
    Serial.print(" ");
  }
  Serial.println("");
}
